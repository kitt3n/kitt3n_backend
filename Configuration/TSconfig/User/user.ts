## PageTs user configurations
options {
    ## Cache options for user
    clearCache.pages = 1
    # clearCache.all = 1

    ## Context menus
    # contextMenu.table.pages.disableItems = new,newWizard

    ## Save and view
    # saveDocNew = 1
    # saveDocNew.tt_content = 1
    # saveDocNew.pages = 1

    ## Makes sure the clipboard doesn't clear
    # saveClipboard = 1

    ## Reduces the amount of clipboards from 4 to 1
    clipboardNumberPads = 1

    ## Checkboxes at the bottom of list views
    file_list.enableClipBoard = activated
    file_list.enableDisplayBigControlPanel = activated
    # file_list.enableDisplayThumbnails = activated

    ## Page tree options
    pageTree.showNavTitle = 1
    pageTree.showDomainNameWithTitle = 1
    pageTree.showPageIdWithTitle = 1
    # pageTree.backgroundColor.0 = #fafafa
}

## Admin panel
admPanel {
    enable.edit = 0

    ## Force re-loading the cache for external TS
    override.tsdebug.forceTemplateParsing = 0
}